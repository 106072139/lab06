//Get current URL
var cur_Url = location.search;
var temp2;

//Two conditions:Public & private chat
var chatid = '';

if (cur_Url.indexOf("?") != -1) {
	var temp1 = cur_Url.split("?");
	temp2 = temp1[1].split("=");
	chatid = temp2[1];
	
	console.log(chatid);
}



function init(){
	
    return new Promise((resolve) => {
        var user_email = '';
        var userId = '';
        var cur_user = '';
        firebase.auth().onAuthStateChanged(function (user) {
            
            // Check user login
            if (user) {
                user_email = user.email;
                userId = user.uid;
                showname.innerHTML = user.email;
                
                var btnLogout= document.getElementById('BtnLogout');
                btnLogout.addEventListener('click', function () {
                    firebase.auth().signOut()
                    .then(function(messege){
                        window.location = "signin.html";
                    })
                    .catch(function(error){
                        console.log("error!");
                    });
                });
                //check username
                firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
                    cur_user = snapshot.val().username;
                    
                    resolve(cur_user);
                }).catch(function(){
                    window.location.assign("username.html");
                });
            } else {
				document.getElementById('chat_message').innerHTML = "";
                window.location.assign("signin.html");
            }
        });
	});
}

function chat(cur_user){
	var user = firebase.auth().currentUser.uid;
	console.log(user);
	
	//data reference
	var username = new Array(2);
	username[0] = cur_user;//sender
	username[1] = chatid;//reveiver
	//make sure they enter the same chatroom
	username.sort();

	if(chatid == 'public'){
		var dataname = 'message/public';
	}else{
		var dataname = 'message/'+username[0]+'_'+username[1]; // first one is sender, second one is receiver.
	}

	//Read Msg and show
	var total_chat=[];
	//自己送出的訊息為綠色,別人傳送的訊息為白色
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username1 = "<div class='my-3 p-3 bg-success rounded box-shadow '><h6 class='border-bottom border-gray pb-2 mb-0' '>";
    var str_after_content = "</p></div></div>\n";
    var str_after_content1 = "</p><img src='img/user.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></div></div>\n";
    var str_middle1 = "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray text-white '><strong class='d-block text-white'>";
    var str_middle = "</h6><div class='media text-muted pt-3'><img src='img/user2.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
	
	var flag = false;
	var chatRef = firebase.database().ref(dataname);
	chatRef.on('value', function(snapshot){
		total_chat=[];
		snapshot.forEach(function (childSnapshot) {
			var value = childSnapshot.val();
			if(value.user == cur_user){
				total_chat.push(str_before_username1 + value.user + str_middle1+ value.message +'</strong>'+ str_after_content1);
				
			}else{
				if(chatid == 'public'){
					total_chat.push(str_before_username + value.user+str_middle+ value.message +'</strong>'+ str_after_content);
					
				}else{
					total_chat.push(str_before_username + value.user+str_middle+ value.message +'</strong>'+ str_after_content);
				}
				
			}
		});
		
		document.getElementById('chat_message').innerHTML = total_chat.join(' ');
		updateScroll();
	});

	//send Msg
	send_btn = document.getElementById('send_btn');
	message_txt = document.getElementById('msg_content');
	send_btn.addEventListener('click', function(){
		if(message_txt.value != ""){
			var message = message_txt.value;
			firebase.database().ref(dataname).push({
				user: cur_user,
				message: message,
				
            });
		}
		message_txt.value = "";
	});

	//下面是存取所有users 建成一個user list
	var str_before_name = "<div id='";
	var str_mid_name = "'>";
	var str_after_name = "</div>";

	//write to show all users
	var total_post = [];
	//write to click that user and jump into private chatroom
	var total_user = [];
	
	var usernameRef = firebase.database().ref("users");

	usernameRef.on('value', function (snapshot) {
		total_post = [];
		snapshot.forEach(function (childSnapshot) {
		var value = childSnapshot.val();
		//Don't show self name
		if(cur_user!=value.username){
			total_post.push(str_before_name + value.username + str_mid_name +value.username + str_after_name);
			total_user.push(value.username);
		}
	});
		//notify
		if(flag){
			Notify();
		}
		else{
			flag = true;
		}
	
		document.getElementById('directuser').innerHTML = total_post.join(' ');
		updateScroll();
               
        total_user.forEach(function(childuser) {
            var chooseUser = document.getElementById(childuser);
            chooseUser.addEventListener('click', function(){  
                window.location.assign("chat.html?id=" + childuser);
            });
        });
    });
}
//New Msg come in and auto scroll it to the bottom
function updateScroll(){
    var element = document.getElementById("chat_message");
    element.scrollTop = element.scrollHeight;
}
//Notification
if (window.Notification) {
	var Notify = function () {
		if (Notification.permission == "granted") {
			var notification = new Notification("Hi!", {
				body: "Someone join Yu's Chat!",
				icon: 'img/user3.png'
			});

			notification.onclick = function () {
				window.open('chat.html?id=public');
			};
		}
	};

	function NotifyMe() {
		if (Notification.permission == "granted") {
			Notify();
		} else if (Notification.permission != "denied") {
			Notification.requestPermission(function (permission) {
				Notify();
			});
		}
	};
	} 
	else {
		alert("Browser doesn't support Notification!");
	}



window.onload = function (){
	if (!('Notification' in window)) {
		console.log('This browser does not support notification');
	  } else {
		console.log("Notification Support on this browser!");
	  }
	
    init()
    .then(cur_user => chat(cur_user));
};
