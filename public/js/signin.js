function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then(function(){
            create_alert("success","歡迎進入Yu's Chat");
            
            window.location = "chat.html?id=public";
        }).catch(function(error) { 
            create_alert("error","");
        }); 
    });

    btnGoogle.addEventListener('click', function () {
         
        var provider = new firebase.auth.GoogleAuthProvider(); 
        firebase.auth().signInWithPopup(provider).then(function(result) {      
            create_alert("success","歡迎進入Yu's Chat");
            
            window.location = "username.html";
        }).catch(function(error) {

          create_alert("error","");
        });       
    });

    btnSignUp.addEventListener('click', function () {   
        
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value)
        .then(function(){
            create_alert("success","註冊成功！");
            window.location = "username.html";
        }).catch(function(error){
            create_alert("error","");
        });
        
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};