# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Yu's Chat
* Key functions (add/delete)
    1. Sign in / Sign up
    2. Host on firebase
    3. Data read & write in authenticated way
    4. RWD
    5. Chat in public
    6. Chat with new user
    7. Load message history
    
* Other functions (add/delete)
    1. CSS animation
    2. Sign in with Google
    3. Chrome notification
    4. Users list
    5. Set user name
    6. Enter key to send message
    7. Auto scroll down 
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-chat-room.firebaseapp.com/

# Components Description : 
1.  sign in / Sign up:若是第一次使用，就要先創建一個新的帳號密碼，然後按下**New Account**，之後會進入 *username.html* 來設置user name，
    之後只要把之前註冊過的帳號密碼打好，按下**Sign in**就會直接跳到 *chat.html*，不需要再設置user name。
2.  Host on firebase:用firebase deploy。
3.  Database read and write:資料都用firebase存取。
4.  RWD:在電腦螢幕上縮放並不會變形。
5.  Chat in public:一開始設置完user name之後會跳到*chat.html*，一進去就是public chatroom，可以直接在那邊聊天，點選左邊的public字樣
    也可以回到public chatroom。
>  自己傳出去的訊息為綠色的，對方傳來的訊息會是白色的。（這邊模仿了line）
<img src="linesimu.png" width="400px" height="300px"></img> <br>
6.  chat with new user:在左邊會有一列user list，可以點選自己想要聊天的對象，之後就會進入你和該user的專屬聊天室。
7.  Load message history:回到public chatroom可以讀取之前的聊天室紀錄。相同的，點選到旁邊user list的隨便一個user就會跳到你跟該user的聊天室，
    一樣可以讀取你跟他之間的聊天記錄。

# Other Functions Description(1~10%) : 
1.  CSS animation:進入*signin.html* 和 *username.html* 的時候會有小動畫。
2.  Sign in with Google:可以用google account登入。
3.  Chrome notification:在登入狀況下，萬一有新的註冊者進來聊天室會寄送通知，告訴大家"someone join Yu's chat"，點擊通知就可以回到public chatroom來看是哪個新user加入聊天室

>  <img src="notify.png" width="390px" height="120px"></img> <br>

4.  User Lists:曾經註冊過的所有users 會列在左邊，並且可以點選與該用戶聊天。
5.  Set user name:創建專屬於自己的user name，並且會跟著該註冊者。
>  (注意：設置 user name 時不能包含空白鍵，中文或是特殊符號等等，否則在點入想要private chat的user時會形成錯誤的chatroom。)
6.  Enter key to send message:打完要傳送的訊息之後可以用鍵盤上的Enter key直接傳送。
7.  Auto scroll down:如果有新訊息，頁面會自動滑到最底部來查看新訊息。



## Security Report (Optional)
只有基本的security實作，使用者一定要使用已註冊過的帳號密碼登入才可以使用本聊天室。

## Reference
1.  聊天室的layout參考:https://chatster-31375.firebaseapp.com/#!/