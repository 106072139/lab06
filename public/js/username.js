function init() {
    var user_email = '';
    var userId = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            user_email = user.email;
            userId = user.uid;
            alert("名稱不可包含空白鍵，中文或者其他符號(!@#$%^&*_-+=)才可以正常體驗本聊天室！");
        } else {
            window.location.assign("signin.html");
        }
    });

    username_btn = document.getElementById('btnUsername');
    username_txt = document.getElementById('username');

    username_btn.addEventListener('click', function(){
        if (username_txt.value != "") {
            
            
            firebase.database().ref('users/'+userId).set({
                email: user_email,
                username: username_txt.value
            })
            .then(
                () => {
                window.location.assign("chat.html?id=public");
            });
        }
    });
}

window.onload = function () {
    
    init();
};
